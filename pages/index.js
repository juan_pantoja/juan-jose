import BasicLayout from "../Layouts/BasicLayout";

export default function Home() {
  return (
    <div>
      <BasicLayout>
        <h1>Estamos en la home</h1>
      </BasicLayout>
    </div>
  );
}
